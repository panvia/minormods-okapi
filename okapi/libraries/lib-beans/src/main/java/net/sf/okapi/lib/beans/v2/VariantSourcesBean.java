package net.sf.okapi.lib.beans.v2;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.VariantSources;
import net.sf.okapi.lib.persistence.IPersistenceSession;
import net.sf.okapi.lib.persistence.PersistenceBean;
import net.sf.okapi.lib.persistence.beans.ReferenceBean;

public class VariantSourcesBean extends PersistenceBean<VariantSources> {

	private Map<String, TextContainerBean> sources =
			new ConcurrentHashMap<String, TextContainerBean>();
	private ReferenceBean defaultSource = new ReferenceBean();
//    private int sourceCount;
    
	@Override
	protected VariantSources createObject(IPersistenceSession session) {
		return new VariantSources(defaultSource.get(TextContainer.class, session));
	}

	@Override
	protected void setObject(VariantSources obj, IPersistenceSession session) {
		for (String locId : sources.keySet()) {
			TextContainerBean tcBean = sources.get(locId);
			obj.set(new LocaleId(locId),
					tcBean.get(TextContainer.class, session)); // increases sourceCount
		}
	}

	@Override
	protected void fromObject(VariantSources obj, IPersistenceSession session) {
//		sourceCount = obj.count();
		for (LocaleId locId	 : obj.getLocales()) {
			TextContainerBean tcBean = new TextContainerBean();
			tcBean.set(obj.get(locId), session);
			
			sources.put(locId.toString(), tcBean);
		}
		defaultSource.set(obj.getDefaultSource(), session);
	}

	public final Map<String, TextContainerBean> getSources() {
		return sources;
	}

	public final void setSources(Map<String, TextContainerBean> sources) {
		this.sources = sources;
	}

	public final ReferenceBean getDefaultSource() {
		return defaultSource;
	}

	public final void setDefaultSource(ReferenceBean defaultSource) {
		this.defaultSource = defaultSource;
	}

//	public final int getSourceCount() {
//		return sourceCount;
//	}
//
//	public final void setSourceCount(int sourceCount) {
//		this.sourceCount = sourceCount;
//	}

}
