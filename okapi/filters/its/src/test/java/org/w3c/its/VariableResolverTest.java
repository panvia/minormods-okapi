/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package org.w3c.its;

import static org.junit.Assert.assertEquals;

import javax.xml.namespace.QName;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class VariableResolverTest {

	@Test
	public void testQuotation () {
		VariableResolver vr = new VariableResolver();
		vr.add(new QName("var1"), "VAR1", false);
		vr.add(new QName("var11"), "Eleven", false);
		vr.add(new QName("var2"), "\"v2\"", false);
		vr.add(new QName("var3"), "'v3'", false);
		assertEquals("\\*[@attr='VAR1']", vr.replaceVariables("\\*[@attr=$var1]"));
		// Test variable with a sub-string name and values with quotes
		assertEquals("a='VAR1' b='Eleven' c='\"v2\"'", vr.replaceVariables("a=$var1 b=$var11 c=$var2"));
		assertEquals("a='Eleven' b='VAR1' c=\"'v3'\"", vr.replaceVariables("a=$var11 b=$var1 c=$var3"));
	}

}
