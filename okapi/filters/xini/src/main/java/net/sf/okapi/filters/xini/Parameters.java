/*===========================================================================
  Copyright (C) 2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.xini;

import net.sf.okapi.common.ISimplifierRulesParameters;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.core.simplifierrules.ParseException;
import net.sf.okapi.core.simplifierrules.SimplifierRules;

public class Parameters extends StringParameters implements ISimplifierRulesParameters {
	/**
	 * Name of the parameter that enables/disables output segmentation
	 */
	public static final String USE_OKAPI_SEGMENTATION = "useOkapiSegmentation";
	
	public Parameters () {
		super();
	}
	
	public void reset () {
		super.reset();
		setUseOkapiSegmentation(true);
		setSimplifierRules(null);
	}

	public boolean isUseOkapiSegmentation() {
		return getBoolean(USE_OKAPI_SEGMENTATION);
	}

	public void setUseOkapiSegmentation(boolean useOkapiSegmentation) {
		setBoolean(USE_OKAPI_SEGMENTATION, useOkapiSegmentation);
	}

	@Override
	public String getSimplifierRules() {
		return getString(SIMPLIFIERRULES);
	}

	@Override
	public void setSimplifierRules(String rules) {
		setString(SIMPLIFIERRULES, rules);		
	}

	@Override
	public void validateSimplifierRules() throws ParseException {
		SimplifierRules r = new SimplifierRules(getSimplifierRules(), new Code());
		r.parse();
	}
	
	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(
				USE_OKAPI_SEGMENTATION, 
				"Use Okapi segmentation for output", 
				"If disabled, all XINI segments with the same value\n" +
				"of the attribute 'SegmentIDBeforeSegmentation' will be merged.\n" +
				"If the XINI was not segmented, it will remain unsegmented.\n" +
				"If this option is enabled, new segmentation\n" +
				"(i.e. from segmentation step) will be used for the output.");
		return desc;
	}
}
