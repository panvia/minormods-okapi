package net.sf.okapi.connectors.microsoft;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.simple.parser.ParseException;

import net.sf.okapi.common.exceptions.OkapiException;

public class DataMarketTokenParserTest {
	private DataMarketTokenParser parser;

	@Before
	public void setup() {
		parser = new DataMarketTokenParser();
	}

	@Test
	public void testThrowExceptionOnError() throws ParseException {
		String json = "{\"error\":\"invalid_client\",\"error_description\":\"ACS50012: Authentication failed.\"}";
		try {
			parser.parse(json);
			fail();
		}
		catch (OkapiException e) {
		}
	}

	private static final String DUMMY_TOKEN = 
			"http%3a%2f%2fschemas.xmlsoap.org%2fws%2f2005%2f05%2fidentity%2fclaims%2fnameidentifier=" +
			"TranslatorHubTest&http%3a%2f%2fschemas.microsoft.com%2faccesscontrolservice%2f2010%2f07%2" +
			"fclaims%2fidentityprovider=https%3a%2f%2fdatamarket.accesscontrol.windows.net%2f&Audience=" +
			"http%3a%2f%2fapi.microsofttranslator.com&ExpiresOn=1461189740&Issuer=https%3a%2f%2fdatamarket" +
			".accesscontrol.windows.net%2f&HMACSHA256=Lb2POowdGM%qwesdd%3d";

	private static final String DUMMY_TOKEN_JSON =
			"{\"token_type\":\"http://schemas.xmlsoap.org/ws/2009/11/swt-token-profile-1.0\"," +
				"\"access_token\":\"" + DUMMY_TOKEN + 
				"\",\"expires_in\":\"600\",\"scope\":\"http://api.microsofttranslator.com\"}";

	@Test
	public void testParseToken() throws ParseException {
		DataMarketTokenParser.Token token = parser.parse(DUMMY_TOKEN_JSON);
		// The token itself returns the expiry time in seconds, which our parser
		// converts to milliseconds
		assertEquals(1461189740L * 1000L, token.expiresOn);
		assertEquals(DUMMY_TOKEN, token.token);
	}

}
