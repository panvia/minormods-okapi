/*===========================================================================
  Copyright (C) 2008-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.common.resource.simplifier;

import java.util.List;

import net.sf.okapi.common.Event;

public interface IEventConverter {

	/**
	 * Converts a given event into a different event, modifying either its type or attached resource.
	 * @param event the given event.
	 * @return a modified event. Can be MULTI_EVENT containing a list of events.
	 */
	Event convert(Event event);
	
	/**
	 * Converts a given event into a list of events.
	 * @param event the given event.
	 * @return a list of events produced by the event converter. The implementation should ensure
	 * that no MULTI_EVENT events are present in the list (MultiEvent resources are "unpacked"). 
	 */
	List<Event> convertToList(Event event);
}
