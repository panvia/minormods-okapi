package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.StartElement;

/**
 * Provides a start element context factory.
 */
class StartElementContextFactory {

    static StartElementContext createStartElementContext(StartElement startElement,
                                                         XMLEventReader eventReader,
                                                         XMLEventFactory eventFactory,
                                                         ConditionalParameters conditionalParameters,
                                                         Class<? extends ElementSkipper.SkippableElement> skippableElementType) {
        return new StartElementContext(startElement, eventReader, eventFactory, conditionalParameters, skippableElementType);
    }

    static StartElementContext createStartElementContext(StartElement startElement,
                                                         XMLEventReader eventReader,
                                                         XMLEventFactory eventFactory,
                                                         ConditionalParameters conditionalParameters) {
        return createStartElementContext(startElement, eventReader, eventFactory, conditionalParameters, null);
    }

    static StartElementContext createStartElementContext(StartElement startElement,
                                                         StartElementContext startElementContext,
                                                         Class<? extends ElementSkipper.SkippableElement> skippableElementType) {
        return createStartElementContext(startElement, startElementContext.getEventReader(), startElementContext.getEventFactory(),
                startElementContext.getConditionalParameters(), skippableElementType);
    }

    static StartElementContext createStartElementContext(StartElement startElement, StartElementContext startElementContext) {
        return createStartElementContext(startElement, startElementContext, null);
    }
}
