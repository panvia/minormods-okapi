package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import static net.sf.okapi.filters.openxml.ElementSkipper.GeneralElementSkipper.skipElementEvents;
import static net.sf.okapi.filters.openxml.Namespaces.WordProcessingML;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.getAttributeValue;

/**
 * Representation of the data in an <w:rFonts> tag, which can be
 * merged with the font information from other runs in some cases.
 */
public class RunFonts {
    static final QName RUN_FONTS = Namespaces.WordProcessingML.getQName("rFonts");

    /**
     * A maximum number of events per rFonts tag.
     */
    private static final int MAX_NUMBER_OF_EVENTS = 2;

    /**
     * Within a single run, there can be up to four types of content present
     * which shall each be allowed to use a unique font.
     */
    private static final int MAX_NUMBER_OF_CONTENT_CATEGORIES = 4;

    private XMLEventFactory eventFactory;
    private StartElement startElement;
    private EnumMap<ContentCategory, String> fonts;

    RunFonts(XMLEventFactory eventFactory, StartElement startElement, EnumMap<ContentCategory, String> fonts) {
        this.eventFactory = eventFactory;
        this.startElement = startElement;
        this.fonts = fonts;
    }

    static RunFonts build(StartElementContext startElementContext) throws XMLStreamException {
        EnumMap<ContentCategory, String> fonts = new EnumMap<>(ContentCategory.class);

        for (ContentCategory contentCategory : ContentCategory.values()) {
            fonts.put(contentCategory, getAttributeValue(startElementContext.getStartElement(), contentCategory.getValue()));
        }

        // Read the closing element as well
        skipElementEvents(startElementContext);

        return new RunFonts(startElementContext.getEventFactory(), startElementContext.getStartElement(), fonts);
    }

    /**
     * Fonts can be merged if they contain no contradictory font information. This means
     * all content categories either have the same value, or else be specified for at most one
     * of the two font objects.
     *
     * @param runFonts The run fonts to check against
     *
     * @return {@code true} if current run fonts can be merged with another
     */
    boolean canBeMerged(RunFonts runFonts) {
        for (ContentCategory category : ContentCategory.values()) {

            if (ContentCategory.HINT == category) {
                continue;
            }

            if (!canContentCategoriesBeMerged(fonts.get(category), runFonts.fonts.get(category))) {
                return false;
            }
        }

        return true;
    }

    private boolean canContentCategoriesBeMerged(String category1, String category2) {
        return (category1 == null && category2 != null)
                || (category1 != null && category2 == null)
                || Objects.equals(category1, category2);
    }

    /**
     * Merges another run fonts object into this one. Returns the merged instance,
     * which may not be the same as this.
     *
     * @param runFonts The run fonts to merge with
     *
     * @return Merged current run fonts
     */
    RunFonts merge(RunFonts runFonts) {
        EnumMap<ContentCategory, String> newFonts = new EnumMap<>(ContentCategory.class);

        for (ContentCategory category : ContentCategory.values()) {
            newFonts.put(category, mergeContentCategories(fonts.get(category), runFonts.fonts.get(category)));
        }

        fonts = newFonts;

        return this;
    }

    private String mergeContentCategories(String category1, String category2) {
        return category1 != null ? category1 : category2;
    }

    List<XMLEvent> getEvents() {
        List<XMLEvent> events = new ArrayList<>(MAX_NUMBER_OF_EVENTS);

        events.add(eventFactory.createStartElement(startElement.getName(), getAttributes(), startElement.getNamespaces()));
        events.add(eventFactory.createEndElement(startElement.getName(), startElement.getNamespaces()));

        return events;
    }

    private Iterator<Attribute> getAttributes() {
        List<Attribute> attributes = new ArrayList<>(MAX_NUMBER_OF_CONTENT_CATEGORIES);

        for (ContentCategory category : ContentCategory.values()) {
            String value = fonts.get(category);

            if (value != null) {
                attributes.add(eventFactory.createAttribute("w", Namespaces.WordProcessingML.getURI(), category.getValue().getLocalPart(), value));
            }
        }

        return attributes.iterator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RunFonts runFonts = (RunFonts) o;
        return Objects.equals(fonts, runFonts.fonts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fonts);
    }

    private enum ContentCategory {
        ASCII("ascii"),
        ASCII_THEME("asciiTheme"),
        HIGH_ANSI("hAnsi"),
        HIGH_ANSI_THEME("hAnsiTheme"),
        COMPLEX_SCRIPT("cs"),
        COMPLEX_SCRIPT_THEME("cstheme"),
        EAST_ASIAN("eastAsia"),
        EAST_ASIAN_THEME("eastAsiaTheme"),
        HINT("hint");

        private QName value;

        ContentCategory(String value) {
            this.value = WordProcessingML.getQName(value);
        }

        public QName getValue() {
            return value;
        }
    }
}
