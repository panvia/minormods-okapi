package net.sf.okapi.filters.openxml;

import java.util.Arrays;
import java.util.List;

/**
 * Provides clarifier strategy factory values.
 */
class ClarifierStrategyFactoryValues {

    static final String EMPTY_ATTRIBUTE_PREFIX = "";

    static final List<String> RTL_BOOLEAN_VALUES = Arrays.asList(
            XMLEventHelpers.BooleanAttributeValue.TRUE_STRING.getValue(),
            XMLEventHelpers.BooleanAttributeValue.TRUE_INTEGER.getValue());
}
