package net.sf.okapi.filters.openxml;

import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;

class StyledText implements Chunk {
    protected StartElement startElement;
    protected Characters text;
    protected EndElement endElement;

    StyledText(StartElement startElement, Characters text, EndElement endElement) {
        this.startElement = startElement;
        this.text = text;
        this.endElement = endElement;
    }

    @Override
    public List<XMLEvent> getEvents() {
        List<XMLEvent> events = new ArrayList<>(3);
        events.add(startElement);
        events.add(text);
        events.add(endElement);
        return events;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[" + XMLEventSerializer.serialize(startElement) + "](" + text + ")";
    }

    public String getText() {
        return text.getData();
    }
}
