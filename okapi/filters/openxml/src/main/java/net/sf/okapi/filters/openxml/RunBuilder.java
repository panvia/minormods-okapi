package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;

import static net.sf.okapi.filters.openxml.ElementSkipper.GeneralElementSkipper.isAlternateContentFallbackStartElement;
import static net.sf.okapi.filters.openxml.ElementSkipper.GeneralElementSkipper.isSoftHyphenStartElement;
import static net.sf.okapi.filters.openxml.ElementSkipper.GeneralElementSkipper.skipElementEvents;
import static net.sf.okapi.filters.openxml.MarkupComponentFactory.createGeneralMarkupComponent;
import static net.sf.okapi.filters.openxml.StartElementContextFactory.createStartElementContext;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.createQName;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.hasPreserveWhitespace;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isEndElement;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isLineBreakStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isNoBreakHyphenStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isTabStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isTextStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isWhitespace;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_REGULAR_HYPHEN_VALUE;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.CACHED_PAGE_BREAK;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_TEXT;

class RunBuilder {
    private StartElementContext startElementContext;
    private EndElement endEvent;
    private RunProperties runProperties = RunProperties.emptyRunProperties();
    private RunProperties combinedRunProperties;
    private List<Chunk> runBodyChunks = new ArrayList<>();
    private List<Textual> nestedTextualItems = new ArrayList<>();
    private boolean isHidden = false;

    private QName textName;
    private String runStyle;
    private boolean containsNestedItems = false;
    private boolean hasComplexCodes;
    private StyleDefinitions styleDefinitions;
    private boolean isTextPreservingWhitespace = false;
    private List<XMLEvent> currentMarkupChunk = new ArrayList<>();
    private boolean hasAnyText = false;
    private boolean hasNonWhitespaceText = false;
    private StringBuilder textContent = new StringBuilder();

    RunBuilder(StartElementContext startElementContext, StyleDefinitions styleDefinitions) {
        this.startElementContext = startElementContext;
        this.styleDefinitions = styleDefinitions;
    }

    StartElementContext getStartElementContext() {
        return startElementContext;
    }

    void setEndEvent(EndElement endEvent) {
        this.endEvent = endEvent;
    }

    List<Chunk> getRunBodyChunks() {
        return runBodyChunks;
    }

    RunProperties getRunProperties() {
        return runProperties;
    }

    void setRunProperties(RunProperties runProperties) {
        this.runProperties = runProperties;
    }

    RunProperties getCombinedRunProperties(String paragraphStyle) {
        if (null == combinedRunProperties) {
            resetCombinedRunProperties(paragraphStyle);
        }

        return combinedRunProperties;
    }

    void resetCombinedRunProperties(String paragraphStyle) {
        combinedRunProperties = styleDefinitions.getCombinedRunProperties(paragraphStyle, runStyle, runProperties);
    }

    List<Textual> getNestedTextualItems() {
        return nestedTextualItems;
    }

    void setHidden(boolean hidden) {
        isHidden = hidden;
    }

    QName getTextName() {
        return textName;
    }

    String getRunStyle() {
        return runStyle;
    }

    void setRunStyle(String runStyle) {
        this.runStyle = runStyle;
    }

    boolean containsNestedItems() {
        return containsNestedItems;
    }

    void setContainsNestedItems(boolean containsNestedItems) {
        this.containsNestedItems = containsNestedItems;
    }

    boolean hasComplexCodes() {
        return hasComplexCodes;
    }

    void setHasComplexCodes(boolean hasComplexCodes) {
        this.hasComplexCodes = hasComplexCodes;
    }

    StyleDefinitions getStyleDefinitions() {
        return styleDefinitions;
    }

    boolean isTextPreservingWhitespace() {
        return isTextPreservingWhitespace;
    }

    void setTextPreservingWhitespace(boolean textPreservingWhitespace) {
        isTextPreservingWhitespace = textPreservingWhitespace;
    }

    boolean hasNonWhitespaceText() {
        return hasNonWhitespaceText;
    }

    Block.BlockChunk build() throws XMLStreamException {
        return new Run(startElementContext.getStartElement(), endEvent,
                runProperties, combinedRunProperties,
                runBodyChunks, nestedTextualItems, isHidden);
    }

    void addRunBody(XMLEvent e, XMLEventReader events) throws XMLStreamException {
        if (isTextStartEvent(e)) {
            flushMarkupChunk();
            processText(e.asStartElement(), events);
        } else if (startElementContext.getConditionalParameters().getAddTabAsCharacter() && isTabStartEvent(e)) {
            flushMarkupChunk();
            addRawText("\t", e.asStartElement(), events);
        } else if (startElementContext.getConditionalParameters().getAddLineSeparatorCharacter() && isLineBreakStartEvent(e)) {
            flushMarkupChunk();
            addRawText("\n", e.asStartElement(), events);
        } else if (e.isStartElement() && isStrippableRunBodyElement(e.asStartElement())) {
            // Consume to the end of the element
            skipElementEvents(createStartElementContext(e.asStartElement(), events, null, startElementContext.getConditionalParameters()));
        } else if (startElementContext.getConditionalParameters().getReplaceNoBreakHyphenTag() && isNoBreakHyphenStartEvent(e)) {
            flushMarkupChunk();
            addRawText(LOCAL_REGULAR_HYPHEN_VALUE, e.asStartElement(), events);
        } else if (startElementContext.getConditionalParameters().getIgnoreSoftHyphenTag() && isSoftHyphenStartElement(e)) {
            // Ignore soft hyphens
            skipElementEvents(createStartElementContext(e.asStartElement(), events, null, startElementContext.getConditionalParameters()));
        } else if (isAlternateContentFallbackStartElement(e)) {
            skipElementEvents(createStartElementContext(e.asStartElement(), events, null, startElementContext.getConditionalParameters()));
        } else if (!isWhitespace(e) || inPreservingWhitespaceElement()) {
            // Real text should have been handled above.  Most whitespace is ignorable,
            // but if we're in a preserve-whitespace section, we need to save it (eg
            // for w:instrText, which isn't translatable but needs to be preserved).
            flushText();
            isTextPreservingWhitespace = false;
            addToMarkupChunk(e);
        }
    }

    /**
     * Handle cases like <w:instrText> -- non-text elements that we may need
     * to obey xml:space="preserve" on.
     */
    private boolean inPreservingWhitespaceElement() {
        // Look only at the most recent element on the stack
        if (currentMarkupChunk.size() > 0) {
            XMLEvent e = currentMarkupChunk.get(currentMarkupChunk.size() - 1);
            if (e instanceof StartElement && hasPreserveWhitespace(e.asStartElement())) {
                return true;
            }
        }
        return false;
    }

    private void processText(StartElement startEvent, XMLEventReader events) throws XMLStreamException {
        hasAnyText = true;
        // Merge the preserve whitespace flag
        isTextPreservingWhitespace = isTextPreservingWhitespace || hasPreserveWhitespace(startEvent);

        if (textName == null) {
            textName = startEvent.getName();
        }
        while (events.hasNext()) {
            XMLEvent e = events.nextEvent();
            if (isEndElement(e, startEvent)) {
                return;
            } else if (e.isCharacters()) {
                String text = e.asCharacters().getData();
                if (text.trim().length() > 0) {
                    hasNonWhitespaceText = true;
                }
                textContent.append(text);
            }
        }
    }

    void flushText() {
        // It seems like there may be a bug where presml runs need to have
        // an empty <a:t/> at a minimum.
        if (hasAnyText) {
            runBodyChunks.add(new Run.RunText(createTextStart(),
                    startElementContext.getEventFactory().createCharacters(textContent.toString()),
                    startElementContext.getEventFactory().createEndElement(textName, null)));
            textContent.setLength(0);
            hasAnyText = false;
        }
    }

    private StartElement createTextStart() {
        return startElementContext.getEventFactory().createStartElement(textName,
                // DrawingML <a:t> does not use the xml:space="preserve" attribute
                isTextPreservingWhitespace && !Namespaces.DrawingML.containsName(textName) ?
                        java.util.Collections.singleton(
                                startElementContext.getEventFactory().createAttribute("xml", Namespaces.XML.getURI(), "space", "preserve"))
                                .iterator() : null,
                null);
    }

    private boolean isStrippableRunBodyElement(StartElement el) {
        return el.getName().equals(CACHED_PAGE_BREAK);
    }

    private void addRawText(String text, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException {
        hasAnyText = true;
        textContent.append(text);
        if (startElement != null) {
            skipElementEvents(createStartElementContext(startElement, eventReader, null, startElementContext.getConditionalParameters()));
        }
        if (textName == null) {
            textName = createQName(LOCAL_TEXT, startElement.getName());
        }
    }

    void addToMarkupChunk(XMLEvent event) {
        currentMarkupChunk.add(event);
    }

    void flushMarkupChunk() {
        if (currentMarkupChunk.size() > 0) {
            runBodyChunks.add(new Run.RunMarkup().addComponent(createGeneralMarkupComponent(currentMarkupChunk)));
            currentMarkupChunk = new ArrayList<>();
        }
    }

    @Override
    public String toString() {
        try {
            return "RunBuilder for " + build().toString();
        } catch (XMLStreamException e) {
            return "RunBuilder (" + e.getMessage() + ")";
        }
    }
}
