package net.sf.okapi.connectors.microsoft;

import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.okapi.common.exceptions.OkapiException;

public class DataMarketTokenParser {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private JSONParser parser = new JSONParser();

	/**
	 * Holds the (URL-encoded) access token and an expiration time, in the
	 * form of a raw Date value.
	 */
	public static class Token {
		final String token;
		final long expiresOn;
		public Token(String token, long expiresOn) {
			this.token = token;
			this.expiresOn = expiresOn;
		}
	}

	public Token parse(String json) throws ParseException {
		JSONObject o = (JSONObject)parser.parse(json);

		if (o.containsKey("error")) {
			String error = (String)o.get("error");
			String description = (String)o.get("error_description");
			logger.error("Unable to authenticate: error={}, description='{}'", error, description);
			throw new OkapiException("Unable to authenticate with Microsoft Translator: error: " + error
						+ ", description: " + description); 
		}
		String accessToken = expectString(o, "access_token", json);
		// We could also use the expires_in key here, but the connector is currently written to
		// work this way
		long expiresOn = findExpiresOnURLParameter(accessToken); 
		return new Token(accessToken, expiresOn * 1000L);
	}

	private String expectString(JSONObject o, String key, String rawJson) {
		if (!o.containsKey(key)) {
			throw new OkapiException("Unexpected authentication token response (missing key: " +
									 key + ": " + rawJson);
		}
		return (String)o.get(key);
	}

	private long findExpiresOnURLParameter(String tokenAsEscapedUrl) {
		try {
			List<NameValuePair> pairs = URLEncodedUtils.parse(tokenAsEscapedUrl, StandardCharsets.UTF_8);
			for (NameValuePair p : pairs) {
				if ("ExpiresOn".equals(p.getName())) {
					return Long.valueOf(p.getValue());
				}
			}
		}
		catch (Exception e) {
			throw new OkapiException("Couldn't extract ExpiresOn value from token: " + tokenAsEscapedUrl, e);
		}
		throw new OkapiException("Couldn't extract ExpiresOn value from token: " + tokenAsEscapedUrl);
	}
}
