package net.sf.okapi.filters.openxml;

interface Skipper<T> {
    boolean canBeSkipped(T t);
}
