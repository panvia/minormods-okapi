package net.sf.okapi.lib.languagetool.rules;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.lib.languagetool.LanguageToolUtil;

public class LanguageToolUtilTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void miscLanguages() {
		assertEquals("en-US", LanguageToolUtil.getLanguage(new LocaleId("en", "US")).getShortNameWithCountryAndVariant());
		assertEquals("en-GB", LanguageToolUtil.getLanguage(new LocaleId("en", "GB")).getShortNameWithCountryAndVariant());

		assertEquals("fr", LanguageToolUtil.getLanguage(LocaleId.FRENCH).getShortNameWithCountryAndVariant());
		assertEquals("fr", LanguageToolUtil.getLanguage(new LocaleId("fr", "FR")).getShortNameWithCountryAndVariant());
		
		assertEquals("ja-JP", LanguageToolUtil.getLanguage(LocaleId.JAPANESE).getShortNameWithCountryAndVariant());		
		
		assertEquals("es", LanguageToolUtil.getLanguage(LocaleId.SPANISH).getShortNameWithCountryAndVariant());
		assertEquals("es", LanguageToolUtil.getLanguage(new LocaleId("es", "MX")).getShortNameWithCountryAndVariant());
		
		assertEquals("zh-CN", LanguageToolUtil.getLanguage(new LocaleId("zh", "CN")).getShortNameWithCountryAndVariant());		
		// hum, this really should be zh-TW but LT only returns zh-CN
		assertEquals("zh-CN", LanguageToolUtil.getLanguage(new LocaleId("zh", "TW")).getShortNameWithCountryAndVariant());		
		assertEquals("default", LanguageToolUtil.getLanguage(LocaleId.ARABIC).getShortNameWithCountryAndVariant());
	}
	
	@Test
	public void portuguese() {
		assertEquals("pt-PT", LanguageToolUtil.getLanguage(new LocaleId("pt", "PT")).getShortNameWithCountryAndVariant());
		assertEquals("pt-BR", LanguageToolUtil.getLanguage(new LocaleId("pt", "BR")).getShortNameWithCountryAndVariant());
	}
	
	@Test
	public void german() {
		assertEquals("de-DE", LanguageToolUtil.getLanguage(LocaleId.GERMAN).getShortNameWithCountryAndVariant());
		assertEquals("de-DE", LanguageToolUtil.getLanguage(new LocaleId("de", "DE")).getShortNameWithCountryAndVariant());
		assertEquals("de-DE", LanguageToolUtil.getLanguage(new LocaleId("de", "BE")).getShortNameWithCountryAndVariant());
		assertEquals("de-DE", LanguageToolUtil.getLanguage(new LocaleId("de", "LI")).getShortNameWithCountryAndVariant());
		assertEquals("de-DE", LanguageToolUtil.getLanguage(new LocaleId("de", "LU")).getShortNameWithCountryAndVariant());
	}
}
