/*===========================================================================
  Copyright (C) 2009-2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.json;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.TestUtil;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filters.InputDocument;
import net.sf.okapi.common.filters.RoundTripComparison;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.StartDocument;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class JSONFilterTest {

	private JSONFilter filter;
	private String root;
	private LocaleId locEN = LocaleId.fromString("en");

	@Before
	public void setUp() {
		filter = new JSONFilter();
		root = TestUtil.getParentDir(this.getClass(), "/test01.json");
	}

	@After
	public void tearDown() {
		filter.close();
	}

	@Test
	public void testList () {
		Parameters params = new Parameters();
		params.setExtractStandalone(true);
		String snippet = "{\"key\" : [ \"Text1\" ] }";
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet, params), 1);
		assertTrue(tu!=null);
	}

	@Test
	public void testObject () {
		String snippet = "{\"key\" : { \"key2\" : \"Text1\" } }";
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet, null), 1);
		assertTrue(tu!=null);
	}
	
	@Test
	public void testValue () {
		String snippet = "{\"key\" : \"Text1\"}";
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet, null), 1);
		assertTrue(tu!=null);
	}
	
	@Test
	public void testAllWithKeyNoException () {
		String snippet = "{ \"key1\" : \"Text1\" }";
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet, null), 1);
		assertNotNull(tu);
		assertEquals("Text1", tu.getSource().toString());
		assertEquals("key1", tu.getName());
	}
	
	@Test
	public void testAllWithKeywithException () {
		String snippet = "{ \"key1\" : \"Text1\" }";
		Parameters params = new Parameters(); // Default: all with keys
		params.setExceptions("key?"); // Except those
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet, params), 1);
		assertTrue(tu==null);
	}
	
	@Test
	public void testNoneWithKeywithException () {
		String snippet = "{ \"key1\" : \"Text1\" }";
		Parameters params = new Parameters();
		params.setExtractAllPairs(false); // None with key
		params.setExceptions("key?"); // Except those
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet, params), 1);
		assertNotNull(tu);
		assertEquals("Text1", tu.getSource().toString());
		assertEquals("key1", tu.getName());
	}
	
	@Test
	public void testPath () {
		String snippet = "{\"key\" : [{ \"key1\" : \"Text1\"}, {\"key2\" : \"Text1\"}] }";
		Parameters params = new Parameters();
		params.setUseKeyAsName(true);
		params.setUseFullKeyPath(true);
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet, params), 1);
		assertTrue(tu!=null);
		assertNotNull(tu.getName());
	}
	
	@Test
	public void testStandaloneYes () {
		String snippet = "{ \"key\" : [ \"Text1\", \"Text2\" ] }";
		Parameters params = new Parameters();
		params.setExtractStandalone(true);
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet, params), 1);
		assertNotNull(tu);
		assertEquals("Text1", tu.getSource().toString());
		assertEquals("key", tu.getName());
		tu = FilterTestDriver.getTextUnit(getEvents(snippet, params), 2);
		assertNotNull(tu);
		assertEquals("Text2", tu.getSource().toString());
	}
	
	@Test
	public void testStandaloneDefaultWhichIsNo () {
		String snippet = "{ \"key\" : [[ \"Text1\" ], [ \"Text2\" ]] }";
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet, null), 1);
		assertTrue(tu==null);
		tu = FilterTestDriver.getTextUnit(getEvents(snippet, null), 2);
		assertTrue(tu==null);
	}
	
	@Test
	public void testSmartQuotes() {
		String snippet = "{ \"key1\" : \"What is (and 'isn’t) special about science\" }";
		Parameters p = new Parameters();
		p.setSubfilter("okf_html");
		List<Event> events = getEvents(snippet, p);
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("What is (and 'isn’t) special about science", tu.getSource().toString());
		assertEquals("key1_1", tu.getName());
		String expected = "{ \"key1\" : \"What is (and &#39;isn’t) special about science\" }";
		// Output is raw character because it's UTF-8
		assertEquals(expected, FilterTestDriver.generateOutput(events,
			filter.getEncoderManager(), locEN));
	}
	
	@Test
	public void testEscape () {
		String snippet = "{ \"key1\" : \"agrave=\\u00E0\" }";
		List<Event> events = getEvents(snippet, null);
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("agrave=\u00e0", tu.getSource().toString());
		assertEquals("key1", tu.getName());
		String expected = "{ \"key1\" : \"agrave=\u00e0\" }";
		// Output is raw character because it's UTF-8
		assertEquals(expected, FilterTestDriver.generateOutput(events,
			filter.getEncoderManager(), locEN));
	}
	
	@Test
	public void testEscapes () {
		String snippet = "  {\n  \"key1\" :  \"abc\\n\\\"\\\\ \\b\\f\\t\\r\\/\"  } \r ";
		List<Event> events = getEvents(snippet, null);
		// Content = '\"'
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertEquals("abc\n\"\\ \b\f\t\r/", tu.getSource().toString());
		assertEquals(snippet, FilterTestDriver.generateOutput(events,
			filter.getEncoderManager(), locEN));
	}
	
	@Test
	public void testWhiteSpaceAndComments () {
		String snippet = "/*comment*/<!--comment-->#comment\n //comment\r\n { #comment\n    \"key\"     :     //comment\n \n\n\n \n  { \"key2\" : /*comment*/ \"<!--html in value comment-->value\"}  } ";
		List<Event> events = getEvents(snippet, null);
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertEquals("<!--html in value comment-->value", tu.getSource().toString());
		assertEquals(snippet, FilterTestDriver.generateOutput(events,
			filter.getEncoderManager(), locEN));
	}

	@Test
	public void testMultilineComment() {
		String snippet = "/*comment*/<!--comment-->{\"key\" :  { \"key2\" : \"value\"}  } ";
		List<Event> events = getEvents(snippet, null);
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertEquals("value", tu.getSource().toString());
		assertEquals(snippet, FilterTestDriver.generateOutput(events,
			filter.getEncoderManager(), locEN));
	}
	
	@Test
	public void testNestedComments () {
		String snippet = "/*c1/*c2/*c3*/*/*/{\"key\" : { \"key2\" : \"value\"}} ";
		List<Event> events = getEvents(snippet, null);
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertEquals("value", tu.getSource().toString());
		assertEquals(snippet, FilterTestDriver.generateOutput(events,
			filter.getEncoderManager(), locEN));
	}
	
	@Test
	public void testEmptyValue() {
		String snippet = "{\"templateOverridePath\": \"\"}";
		List<Event> events = getEvents(snippet, null);
		assertEquals(snippet, FilterTestDriver.generateOutput(events,
			filter.getEncoderManager(), locEN));
	}
	
	@Test
	public void testDecimalNumber() {
		String snippet = "{\"northeast\":{\"lat\":37.8302885,\"lng\":-122.4766272}}";
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet, null), 1);
		assertTrue(tu==null);
		tu = FilterTestDriver.getTextUnit(getEvents(snippet, null), 2);
		assertTrue(tu==null);
	}
	
	@Test
	public void testDoubleExtraction () throws URISyntaxException {
		ArrayList<InputDocument> list = new ArrayList<InputDocument>();
		list.add(new InputDocument(root+"test01.json", null));
		list.add(new InputDocument(root+"test02.json", null));
		list.add(new InputDocument(root+"test03.json", null));
		list.add(new InputDocument(root+"test04.json", null));
		list.add(new InputDocument(root+"test05.json", null));
		list.add(new InputDocument(root+"test06.json", null));
		list.add(new InputDocument(root+"test08.json", null));
		list.add(new InputDocument(root+"test09.json", null));
		list.add(new InputDocument(root+"books.json", null));
		list.add(new InputDocument(root+"geo.json", null));
		list.add(new InputDocument(root+"array-test.json", null));
		list.add(new InputDocument(root+"twitter.json", null));

		RoundTripComparison rtc = new RoundTripComparison();
		assertTrue(rtc.executeCompare(filter, list, "UTF-8", locEN, locEN));
	}
	
	@Test
	public void testDoubleExtractionOnPreviousFailure () throws URISyntaxException {
		ArrayList<InputDocument> list = new ArrayList<InputDocument>();
		list.add(new InputDocument(root+"customer_form.json", null));
		
		RoundTripComparison rtc = new RoundTripComparison();
		assertTrue(rtc.executeCompare(filter, list, "UTF-8", locEN, locEN));
	}

	@Test
	public void testDoubleExtractionOnInvalid () throws URISyntaxException {
		ArrayList<InputDocument> list = new ArrayList<InputDocument>();
		list.add(new InputDocument(root+"invalid_by_most_processors.json", null));
		
		RoundTripComparison rtc = new RoundTripComparison();
		assertTrue(rtc.executeCompare(filter, list, "UTF-8", locEN, locEN));
	}
	
	@Test
	public void testDefaultInfo () {
		assertNotNull(filter.getParameters());
		assertNotNull(filter.getName());
		assertNotNull(filter.getDisplayName());
		List<FilterConfiguration> list = filter.getConfigurations();
		assertNotNull(list);
		assertTrue(list.size()>0);
	}

	@Test
	public void testSimpleEntrySkeleton () {
		String snippet = "  {\r  \"key1\" :  \"Text1\"  } \r ";
		assertEquals(snippet, FilterTestDriver.generateOutput(getEvents(snippet, null),
			filter.getEncoderManager(), locEN));
	}

	@Test
	public void testLineBreaks () {
		String snippet = "{ \"key1\" : \"Text1\" }\r";
		StartDocument sd = FilterTestDriver.getStartDocument(getEvents(snippet, null));
		assertNotNull(sd);
		assertEquals("\r", sd.getLineBreak());
	}
	
	@Test
	public void testSubfilter() throws IOException {
		// This is testing:
		//  - JSON unescaping (\\b to \b, \\n to \n, \" to ")
		//  - JSON output escaping (/ to \/, \b to \\b)
		//  - Subfilter whitespace folding (\t, \n disappear) and output escaping (& to &amp;, " to &quot;)
		//  - Subfilter parsing of block-level (<p>) and inline tags (<i>)
		String snippet = "{ \"key1\" : \"<p>Hello, \\t \\\"<i>crazy</i>\\\" \\b world & friends!\\n\\n<\\/p> \\n!\" }";
		Parameters params = new Parameters();
		params.setSubfilter("okf_html");
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet, params), 1);
		assertNotNull(tu);
		assertEquals("key1_1", tu.getName());
		assertEquals("sg1_tu1", tu.getId());
		assertEquals("Hello, \"<i>crazy</i>\" \b world & friends!", tu.getSource().toString());
		assertEquals("Hello, \"crazy\" \b world & friends!", tu.getSource().getCodedText());
		
		String expected = "{ \"key1\" : \"<p>Hello, &quot;<i>crazy<\\/i>&quot; \\b world &amp; friends!<\\/p>!\" }";
		assertEquals(expected, eventWriter(snippet));
	}
	
	@Test
	public void testSubfilterEasyToDebug() throws IOException {
		// This is testing:
		//  - JSON unescaping (\\b to \b, \\n to \n, \" to ")
		//  - JSON output escaping (/ to \/, \b to \\b)
		//  - Subfilter whitespace folding (\t, \n disappear) and output escaping (& to &amp;, " to &quot;)
		//  - Subfilter parsing of block-level (<p>) and inline tags (<i>)
		String snippet = "{ \"key1\" : \"H\\t\\bH\" }";
		Parameters params = new Parameters();
		params.setSubfilter("okf_html");
		ITextUnit tu = FilterTestDriver.getTextUnit(getEvents(snippet, params), 1);
		assertNotNull(tu);
		assertEquals("key1_1", tu.getName());
		assertEquals("sg1_tu1", tu.getId());
		assertEquals("H \bH", tu.getSource().toString());
		assertEquals("H \bH", tu.getSource().getCodedText());
		
		String expected = "{ \"key1\" : \"H \\bH\" }";
		assertEquals(expected, eventWriter(snippet));
	}
	
	@Test
	public void testSubFilterDoubleExtraction () throws URISyntaxException {
		ArrayList<InputDocument> list = new ArrayList<InputDocument>();
		list.add(new InputDocument(root+"test07-subfilter.json", null));
		
		RoundTripComparison rtc = new RoundTripComparison();
		
		assertTrue(rtc.executeCompare(filter, list, "UTF-8", locEN, locEN));
		
		setSubfilterMapping("net.sf.okapi.filters.html.HtmlFilter");
		Parameters params = new Parameters();
		params.setSubfilter("okf_html");
		filter.setParameters(params);
		
		assertTrue(rtc.executeCompare(filter, list, "UTF-8", locEN, locEN));
	}
	
	private ArrayList<Event> getEvents(String snippet, IParameters params) {
		setSubfilterMapping("net.sf.okapi.filters.html.HtmlFilter");
		return FilterTestDriver.getEvents(filter, snippet, params, locEN, null);
	}
	
	private void setSubfilterMapping(String filterClass) {
		FilterConfigurationMapper mapper = new FilterConfigurationMapper();
        mapper.addConfigurations(filterClass);
        filter.setFilterConfigurationMapper(mapper);
	}
	
	private String eventWriter(String input) throws IOException {
		try (IFilterWriter writer = filter.createFilterWriter()) {
			// Open the input
			filter.open(new RawDocument(input, LocaleId.ENGLISH, LocaleId.SPANISH));

			// Prepare the output
			writer.setOptions(LocaleId.SPANISH, "UTF-8");
			ByteArrayOutputStream writerBuffer = new ByteArrayOutputStream();
			writer.setOutput(writerBuffer);

			// Process the document
			Event event;
			while (filter.hasNext()) {
				event = filter.next();
				writer.handleEvent(event);
			}			
			writerBuffer.close();
			return new String(writerBuffer.toByteArray(), "UTF-8");
		}
	}
}
