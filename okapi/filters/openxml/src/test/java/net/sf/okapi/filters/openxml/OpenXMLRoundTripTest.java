/*===========================================================================
  Copyright (C) 2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.openxml;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.resource.RawDocument;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This tests OpenXMLFilter (including OpenXMLContentFilter) and
 * OpenXMLZipFilterWriter (including OpenXMLContentSkeleton writer)
 * by filtering, automatically translating, and then writing the
 * zip file corresponding to a Word, Excel or Powerpoint 2009 file, 
 * then comparing it to a gold file to make sure nothing has changed.
 * It does this with a specific list of files.
 * 
 * <p>This is done with no translator first, to make sure the same
 * file is created that was filtered in the first place.  Then it
 * is translated into Pig Latin by PigLatinTranslator, translated so
 * codes are expanded by CodePeekTranslator, and then translated to
 * see a view like the translator will see by TagPeekTranslator.
 */

@RunWith(JUnit4.class)
public class OpenXMLRoundTripTest {
	private XMLFactories factories = new XMLFactoriesForTest();
	private LocaleId locENUS = LocaleId.fromString("en-us");

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	private boolean allGood=true;

	@Before
	public void before() throws Exception {
		this.allGood = true;
	}

	@Test
	public void runTestsWithDefaultConfig() {
		ConditionalParameters cparams = getParametersFromUserInterface();

		ArrayList<String> themfiles = new ArrayList<String>();
		themfiles.add("BoldWorld.docx");
		themfiles.add("Deli.docx");
		themfiles.add("DocProperties.docx");
		themfiles.add("Escapades.docx");
		themfiles.add("Addcomments.docx");
		themfiles.add("styles.docx");
		themfiles.add("sample.pptx");
		themfiles.add("sample.xlsx");
		themfiles.add("sampleMore.xlsx");
		themfiles.add("sampleMore.pptx");
		themfiles.add("OpenXML_text_reference_v1_2.docx");
		themfiles.add("Mauris.docx");
		themfiles.add("HiddenExcluded.docx");
		themfiles.add("ExcelColors.xlsx");
		themfiles.add("UTF8.docx");
		themfiles.add("commentTable.xlsx");
		themfiles.add("InsertText.pptx");
		themfiles.add("Endpara.pptx");
		themfiles.add("MissingPara.docx");
		themfiles.add("EndGroup.docx");
		themfiles.add("Practice2.docx");
		themfiles.add("Hangs.docx");
		themfiles.add("TestLTinsideBoxFails.docx");
		themfiles.add("watermark.docx"); // tests translation of watermark
		themfiles.add("word art.docx"); // tests translation of text in word art
		themfiles.add("table of contents - automatic.docx"); // test exclusion of hyperlinks
		themfiles.add("equation.docx");
		themfiles.add("docxtest.docx"); // extract from mc:Choice and mc:Fallback
		themfiles.add("docxsegtest.docx"); // extract from mc:Choice and mc:Fallback
 		themfiles.add("SmartArt.pptx"); // extract from mc:Choice and mc:Fallback
 		themfiles.add("neverendingloop.docx"); // issue 350 #3
 		themfiles.add("DrawingML_Test.docx"); // issue
		themfiles.add("sample.docx");
		themfiles.add("TextBoxes.docx");
		themfiles.add("OutOfTheTextBox.docx");
 		themfiles.add("apissue.docx");
 		themfiles.add("GraphicInTextBox.docx"); // issue 350 #1 doesn't work yet
 		themfiles.add("TestDako2.docx");
		themfiles.add("shape with text.docx"); // test translation of text in the shape
		themfiles.add("AlternateContentTest.docx"); // extract from mc:Choice and mc:Fallback
		themfiles.add("AlternateContent.docx");
		themfiles.add("columns.xlsx");
		themfiles.add("NoStylesXml.docx");
		themfiles.add("AltContentEscaping.docx");
		themfiles.add("graphicdata.docx");
		themfiles.add("br.docx");
		themfiles.add("multiple_tabs.docx");
		themfiles.add("smartquotes.docx");
		themfiles.add("br2.docx");
		themfiles.add("table_truncation.docx");
		themfiles.add("delTextAmp.docx");
		themfiles.add("hyperlink.docx");
		themfiles.add("diagrams.pptx");
		themfiles.add("gettysburg_en.docx");
		themfiles.add("textarea.docx");
		themfiles.add("picture.docx");
		themfiles.add("spelling.docx");
		themfiles.add("lang.docx");
		themfiles.add("slideLayouts.pptx");
		themfiles.add("formulas.pptx");
		themfiles.add("spacing.docx");
		themfiles.add("vertAlign.docx");
		themfiles.add("bookmarkgoback.docx");
		themfiles.add("hidden_cells.xlsx");
		themfiles.add("document-with-run-fonts-variations.docx");
		themfiles.add("document-style-definitions.docx");
		themfiles.add("sharedstring_entities.xlsx");
		themfiles.add("hidden_stuff.xlsx");
		themfiles.add("pagebreak.docx");
		themfiles.add("TextboxNumber.docx");
		themfiles.add("Hidden_Textbox.docx");
		themfiles.add("large-attribute.docx");
		themfiles.add("smart_art.docx");
		themfiles.add("document-revision-information-stripping.docx");
		themfiles.add("hidden_table.xlsx");
		themfiles.add("chartAmpersand.docx");
		themfiles.add("issue536.docx");

		for(String s : themfiles)
		{
//     	runOneTest(s,false,false); // English NOTE: run only one of these four at a time, because locENUS changes
 		runOneTest(s,true,false, cparams);  // PigLatin
// 		runOneTest(s,false,true);  // Tags
// 		runOneTest(s,true,true);   // Codes
		}
		assertTrue("Some Roundtrip files failed.",allGood);
	}

	@Test
	public void testHiddenTablesWithFormula() {
		ConditionalParameters cparams = getParametersFromUserInterface();
		cparams.setTranslateExcelHidden(false);
		runOneTest("hidden_table_with_formula.xlsx", true, false, cparams);
		assertTrue("Some Roundtrip files failed.",allGood);
	}

	@Test
	public void testPhoneticRunPropertyForAsianLanguages() {
		ConditionalParameters cparams = getParametersFromUserInterface();
		cparams.setTranslateExcelHidden(false);
		runOneTest("japanese_phonetic_run_property.xlsx", true, false, cparams);
		assertTrue("Some Roundtrip files failed.",allGood);
	}

	@Test
	public void testExternalHyperlinks() {
		ConditionalParameters cparams = getParametersFromUserInterface();
		cparams.setExtractExternalHyperlinks(true);
		runOneTest("external_hyperlink.docx", true, false, cparams);
		runOneTest("external_hyperlink.pptx", true, false, cparams);
		assertTrue("Some Roundtrip files failed.",allGood);
	}

	@Test
	public void testClarifiablePart() throws Exception {
		ConditionalParameters conditionalParameters = getParametersFromUserInterface();

		runOneTest("clarifiable-part-en.pptx", false, false, conditionalParameters, "", LocaleId.ENGLISH);
		runOneTest("clarifiable-part-ar.pptx", false, false, conditionalParameters, "", LocaleId.ARABIC);
		runOneTest("clarifiable-part-en.xlsx", false, false, conditionalParameters, "", LocaleId.ENGLISH);
		runOneTest("clarifiable-part-ar.xlsx", false, false, conditionalParameters, "", LocaleId.ARABIC);
		assertTrue("Some Roundtrip files failed.",allGood);
	}

	@Test
	public void runTestsWithColumnExclusion() throws Exception {
		ConditionalParameters params = new ConditionalParameters();
		params.setTranslateDocProperties(false);
        params.setTranslateExcelExcludeColumns(true);
        params.tsExcelExcludedColumns = new TreeSet<String>();
        params.tsExcelExcludedColumns.add("1A");

        runOneTest("shared_string_in_two_columns.xlsx", true, false, params);
        assertTrue("Some Roundtrip files failed.", allGood);
	}

	// Slimmed-down version of some of the integration tests -- this checks for idempotency
	// by roundtripping once, then using the output of that to roundtrip again.  The first and
	// second roundtrip outputs should be the same.
	@Test
	public void runTestTwice() throws Exception {
		ConditionalParameters params = new ConditionalParameters();
		params.setTranslateDocProperties(false);
		runTestTwice("Escapades.docx", params);
	}

	@Test
	public void runTestsExcludeGraphicMetaData() {
		ConditionalParameters params = new ConditionalParameters();
		params.setTranslateWordExcludeGraphicMetaData(true);
		runTests("exclude_graphic_metadata/", params,
				"textarea.docx",
				"picture.docx");
		assertTrue("Some Roundtrip files failed.", allGood);
	}

	@Test
	public void runTestsWithAggressiveTagStripping() {
		ConditionalParameters params = new ConditionalParameters();
		params.setCleanupAggressively(true);
		runTests("aggressive/", params,
				 "spacing.docx",
				 "vertAlign.docx");
		assertTrue("Some Roundtrip files failed.", allGood);
	}

	@Test
	public void runTestsWithHiddenCellsExposed() {
		ConditionalParameters params = new ConditionalParameters();
		params.setTranslateExcelHidden(true);
		runTests("hidden_cells/", params, "hidden_cells.xlsx");
		runTests("hidden_cells/", params, "hidden_stuff.xlsx");
		runTests("hidden_cells/", params, "hidden_table.xlsx");
		assertTrue("Some Roundtrip files failed.", allGood);
	}

	@Test
	public void runTestWithStyledTextCell() {
		ConditionalParameters params = new ConditionalParameters();
		params.setTranslateExcelHidden(true);
		runOneTest("styled_cells.xlsx", true, false, params);
	}

	/**
	 * Runs tests for all given files.
	 *
	 * @param files file names
	 */
	private void runTests(String goldSubDirPath, ConditionalParameters params, String... files) {
		for(String s : files)
		{
			runOneTest(s, true, false, params, goldSubDirPath);  // PigLatin
		}
		assertTrue("Some Roundtrip files failed.", allGood);
	}

	@Test
	public void runTestsAddTabAsCharacter() {
		ConditionalParameters params = new ConditionalParameters();
		params.setAddTabAsCharacter(true);

		List<String> files = new ArrayList<>();
		files.add("Document-with-tabs.docx"); // <w:r><w:t>Before</w:t></w:r><w:r><w:tab/><w:t>after.</w:t></w:r>
		files.add("Document-with-tabs-2.docx"); // <w:r><w:t>Before</w:t></w:r><w:r><w:tab/></w:r><w:r w:rsidR="00F961BC"><w:t>after.</w:t></w:r>
		files.add("Document-with-tabs-3.docx"); // runs with tab should not be combined
		files.add("Document-with-tabs-4.docx"); // runs with tab should not be combined
		files.add("Document-with-tabs-5.docx");
		files.add("Document-with-custom-tabs.docx"); // <w:tabs><w:tab w:val="left" w:pos="6999"/></w:tabs>
		files.add("Document-with-tabs-at-EOL.docx");
		files.add("Document-with-tab-09.docx");
		files.add("tabstyles.docx");
		files.add("Document-with-formula-and-tabs.docx");

		// Tabs - PPTX
		files.add("Document-with-tabs.pptx");

		runTests("tabsaschar/", params, files.toArray(new String[0]));
	}

	@Test
	public void runTestsAddLineSeparatorCharacter() {
		ConditionalParameters params = new ConditionalParameters();
		params.setAddLineSeparatorCharacter(true);

		List<String> files = new ArrayList<>();
		files.add("Document-with-soft-linebreaks.docx");
		//files.add("Document-with-soft-linebreaks.pptx");
		files.add("pagebreak.docx");

		runTests("lbaschar/", params, files.toArray(new String[0]));
	}

	public void runOneTest (String filename, boolean bTranslating, boolean bPeeking, ConditionalParameters cparams) {
		runOneTest(filename, bTranslating, bPeeking, cparams, "");
	}

	public void runOneTest (String filename, boolean bTranslating, boolean bPeeking, ConditionalParameters cparams,
							String goldSubdirPath) {
		runOneTest(filename, bTranslating, bPeeking, cparams, goldSubdirPath, locENUS);
	}

	public void runOneTest (String filename, boolean bTranslating, boolean bPeeking, ConditionalParameters cparams,
						    String goldSubdirPath, LocaleId localeId) {
		Path sInputPath, sOutputPath, sGoldPath;
		Event event;
		URI uri;
		OpenXMLFilter filter = null;
		boolean rtrued2;
		try {
			if (bPeeking)
			{
				if (bTranslating)
					filter = new OpenXMLFilter(new CodePeekTranslator(), localeId);
				else
					filter = new OpenXMLFilter(new TagPeekTranslator(), localeId);
			}
			else if (bTranslating) {
				localeId = new LocaleId("en-US",false); // don't lower-case the US
				filter = new OpenXMLFilter(new PigLatinTranslator(), localeId);
//				filter = new OpenXMLFilter(new PigLatinTranslator(), locLA);
			}
			else
				filter = new OpenXMLFilter();
			
			filter.setParameters(cparams);
			filter.setOptions(locENUS, "UTF-8", true);

			sInputPath = Paths.get(getClass().getResource("/BoldWorld.docx").toURI()).getParent();
			sOutputPath = sInputPath.resolve("output");
			sGoldPath = sInputPath.resolve("gold").resolve(goldSubdirPath);

			uri = sInputPath.resolve(filename).toUri();
			
			try
			{
				filter.open(new RawDocument(uri,"UTF-8", localeId));
			}
			catch(Exception e)
			{
				throw new OkapiException(e);				
			}
			
			OpenXMLZipFilterWriter writer = new OpenXMLZipFilterWriter(cparams,
						factories.getInputFactory(), factories.getOutputFactory(), factories.getEventFactory());

			if (bPeeking)
				writer.setOptions(localeId, "UTF-8");
			else if (bTranslating)
				writer.setOptions(localeId, "UTF-8");
//				writer.setOptions(locLA, "UTF-8");
			else
				writer.setOptions(localeId, "UTF-8");

			String writerFilename = bPeeking ? (bTranslating ? "Peek" : "Tag") : (bTranslating ? "Tran" : "Out")+filename;
			writer.setOutput(sOutputPath.resolve(writerFilename).toString());
			
			while ( filter.hasNext() ) {
				event = filter.next();
				if (event!=null)
				{
					writer.handleEvent(event);
				}
				else
					event = null; // just for debugging
			}
			writer.close();
			Path outputPath = sOutputPath.resolve(
					bPeeking ? (bTranslating ? "Peek" : "Tag") : (bTranslating ? "Tran" : "Out") + filename);
			Path goldPath = sGoldPath.resolve(
					bPeeking ? (bTranslating ? "Peek" : "Tag") : (bTranslating ? "Tran" : "Out") + filename);

			OpenXMLPackageDiffer differ = new OpenXMLPackageDiffer(Files.newInputStream(goldPath),
																   Files.newInputStream(outputPath));
			rtrued2 = differ.isIdentical();
			if (!rtrued2) {
				LOGGER.warn("{}{}{}",
							(bPeeking ? (bTranslating ? "Peek" : "Tag") : (bTranslating ? "Tran" : "Out")),
							filename, (rtrued2 ? " SUCCEEDED" : " FAILED"));
				LOGGER.warn("Gold: {}\nOutput: {}", goldPath, outputPath);
				for (OpenXMLPackageDiffer.Difference d : differ.getDifferences()) {
					LOGGER.warn("+ {}", d.toString());
				}
			}
			if (!rtrued2)
				allGood = false;
			differ.cleanup();
		}
		catch ( Throwable e ) {
			LOGGER.warn("Failed to roundtrip file " + filename, e);
			fail("An unexpected exception was thrown on file '"+filename+e.getMessage());
		}
		finally {
			if ( filter != null ) filter.close();
		}
	}

	public void runTestTwice (String filename, ConditionalParameters cparams) {
		Path sInputPath, sOutputPath;
		URI uri;
		try {
			sInputPath = Paths.get(getClass().getResource("/BoldWorld.docx").toURI()).getParent();
			sOutputPath = sInputPath.resolve("output");

			uri = sInputPath.resolve(filename).toUri();
			String writerFilename = "1_" + filename;
			Path outputPath1 = sOutputPath.resolve(writerFilename);

			roundTrip(uri, writerFilename, cparams);

			String writerFilename2 = "2_" + filename;
			Path outputPath2 = sOutputPath.resolve(writerFilename2);

			roundTrip(outputPath1.toUri(), writerFilename2, cparams);

			OpenXMLPackageDiffer differ = new OpenXMLPackageDiffer(Files.newInputStream(outputPath1),
																   Files.newInputStream(outputPath2));
			boolean same = differ.isIdentical();
			if (!same) {
				LOGGER.warn("{}{}", filename, (same ? " SUCCEEDED" : " FAILED"));
				for (OpenXMLPackageDiffer.Difference d : differ.getDifferences()) {
					LOGGER.warn("+ {}", d.toString());
				}
			}
			differ.cleanup();
			assertTrue(same);
		}
		catch ( Throwable e ) {
			LOGGER.warn("Failed to roundtrip file " + filename, e);
			fail("An unexpected exception was thrown on file '"+filename+e.getMessage());
		}
	}

	private void roundTrip(URI inputUri, String outputFilename, ConditionalParameters cparams) throws Exception {
		OpenXMLFilter filter = new OpenXMLFilter();
		try {
			filter.setParameters(cparams);
			filter.setOptions(locENUS, "UTF-8", true);

			Path sInputPath = Paths.get(getClass().getResource("/BoldWorld.docx").toURI()).getParent();
			Path sOutputPath = sInputPath.resolve("output");

			try {
				filter.open(new RawDocument(inputUri,"UTF-8", locENUS),true); // DWH 7-16-09 squishiness
			}
			catch(Exception e) {
				throw new OkapiException(e);
			}

			OpenXMLZipFilterWriter writer = new OpenXMLZipFilterWriter(cparams,
					factories.getInputFactory(), factories.getOutputFactory(), factories.getEventFactory());
			writer.setOptions(locENUS, "UTF-8");

			writer.setOutput(sOutputPath.resolve(outputFilename).toString());

			while ( filter.hasNext() ) {
				Event event = filter.next();
				if (event != null) {
					writer.handleEvent(event);
				}
			}
			writer.close();
		}
		finally {
			if (filter != null) filter.close();
		}
	}

	private ConditionalParameters getParametersFromUserInterface()
	{
		ConditionalParameters parms;
//    Choose the first to get the UI $$$
//		parms = (new Editor()).getParametersFromUI(new ConditionalParameters());
		parms = new ConditionalParameters();
		return parms;
	}
}
