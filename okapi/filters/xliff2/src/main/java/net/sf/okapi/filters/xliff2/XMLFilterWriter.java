/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
============================================================================*/

package net.sf.okapi.filters.xliff2;

import java.io.IOException;

import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.filterwriter.GenericFilterWriter;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.skeleton.ISkeletonWriter;

public class XMLFilterWriter extends GenericFilterWriter {
	public XMLFilterWriter (ISkeletonWriter skeletonWriter,
		EncoderManager encodermanager)
	{
		super(skeletonWriter, encodermanager);
	}
	
	@Override
	public String getName () {
		return "XmlFilterWriter";
	}	

	@Override
	public ISkeletonWriter getSkeletonWriter () {
		return super.getSkeletonWriter();
	}
	
	@Override
	protected void processTextUnit (ITextUnit resource) throws IOException {
		writer.write(getSkeletonWriter().processTextUnit(resource));
	}
}
