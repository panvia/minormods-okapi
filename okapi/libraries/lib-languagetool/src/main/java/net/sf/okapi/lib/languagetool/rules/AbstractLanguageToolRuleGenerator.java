package net.sf.okapi.lib.languagetool.rules;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.languagetool.AnalyzedToken;
import org.languagetool.AnalyzedTokenReadings;
import org.languagetool.Language;
import org.languagetool.rules.patterns.PatternToken;

import net.sf.okapi.lib.languagetool.LanguageToolUtil;

public abstract class AbstractLanguageToolRuleGenerator {
	protected List<PatternToken> getPatternTokens(String term, Language lang) throws IOException {
		List<PatternToken> patterns = new ArrayList<>();
		List<AnalyzedTokenReadings> tokens = LanguageToolUtil.tokenize(term, lang);
		for (AnalyzedTokenReadings t : tokens) {
			// remove whitespace only tokens
			if (t.isWhitespace() || t.isLinebreak()) {
				continue;
			}
			
			if (t.hasSameLemmas()) {
				// single lemma for the token, we only need one rule
				patterns.add(createPatternToken(t.getAnalyzedToken(0).getTokenInflected()));				
			} else {				
				// multiple possible lemmas, add each one as an "or" rule
				patterns.add(createPatternToken(t.getReadings()));				
			}						
		}
		return patterns;
	}
	
	protected PatternToken createPatternToken(String lemma) {
		PatternToken pt = new PatternToken(lemma, false, false, true);
		pt.setInsideMarker(true);
		pt.setPhraseName("term");
		pt.setMinOccurrence(1);	
		pt.setMaxOccurrence(1);
		return pt;
	}
	
	protected PatternToken createPatternToken(List<AnalyzedToken> ats) {		
		PatternToken pt = new PatternToken(ats.get(0).getTokenInflected(), false, false, true);
		pt.setInsideMarker(true);
		pt.setPhraseName("term");
		pt.setMinOccurrence(1);	
		pt.setMaxOccurrence(1);
		
		// remove the primary lemma and cycle through all others 
		// as "or" matches
		ats.set(0, null);
		HashMap<String, String> duplicates = new HashMap<>();
		for (AnalyzedToken analyzedToken : ats) {
			// don't add the lemma if it is already added
			// since we don't have POS info its the best we can do
			if (analyzedToken == null || duplicates.containsKey(analyzedToken.getTokenInflected())) continue;
			PatternToken pt_or = new PatternToken(analyzedToken.getTokenInflected(), false, false, true);
			pt_or.setInsideMarker(true);
			pt_or.setPhraseName("term");
			pt_or.setMinOccurrence(1);	
			pt_or.setMaxOccurrence(1);
			pt.setOrGroupElement(pt_or);
			duplicates.put(analyzedToken.getTokenInflected(), analyzedToken.getTokenInflected());
		}		
		return pt;
	}
}
