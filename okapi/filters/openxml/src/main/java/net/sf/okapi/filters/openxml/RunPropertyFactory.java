package net.sf.okapi.filters.openxml;


import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import static net.sf.okapi.filters.openxml.Namespaces.WordProcessingML;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.RUN_STYLE;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.gatherEvents;

/**
 * Provides a run property factory.
 */
class RunPropertyFactory {

    private final static int DEFAULT_EVENTS_SIZE = 2;

    /**
     * Toggle property names.
     */
    private static final EnumSet<TogglePropertyName> TOGGLE_PROPERTY_NAMES = EnumSet.range(TogglePropertyName.BOLD, TogglePropertyName.VANISH);

    /**
     * Creates a run property.
     *
     * @param startElementContext   Contains XML event factory, XML event reader and StartElement
     *
     * @return A created run property
     *
     * @throws XMLStreamException
     */
    static RunProperty createRunProperty(StartElementContext startElementContext) throws XMLStreamException {
        if (RunFonts.RUN_FONTS.equals(startElementContext.getStartElement().getName())) {
            return new RunProperty.FontsRunProperty(RunFonts.build(startElementContext));
        } else if (RUN_STYLE.equals(startElementContext.getStartElement().getName())) {
            return new RunProperty.RunStyleProperty(gatherEvents(startElementContext));
        } else if (TOGGLE_PROPERTY_NAMES.contains(TogglePropertyName.fromValue(startElementContext.getStartElement().getName()))) {
            return new RunProperty.ToggleRunProperty(gatherEvents(startElementContext));
        } else {
            return new RunProperty.GenericRunProperty(gatherEvents(startElementContext));
        }
    }

    /**
     * Creates a run property.
     *
     * @param attribute An attribute
     *
     * @return A created run property
     */
    static RunProperty createRunProperty(Attribute attribute) {
        return new RunProperty.AttributeRunProperty(attribute);
    }

    /**
     * Creates a run property.
     *
     * @param creationalParameters Creational parameters
     * @param localName            A local name
     * @param attributes           Attributes
     *
     * @return A created run property
     */
    static RunProperty createRunProperty(CreationalParameters creationalParameters, String localName, Map<String, String> attributes) {
        List<XMLEvent> events = new ArrayList<>(DEFAULT_EVENTS_SIZE);

        List<Attribute> attributeList = new ArrayList<>(attributes.size());

        for (Map.Entry<String, String> attribute : attributes.entrySet()) {
            attributeList.add(creationalParameters.getEventFactory().createAttribute(
                    creationalParameters.getPrefix(), creationalParameters.getNamespaceUri(), attribute.getKey(), attribute.getValue()));
        }

        events.add(creationalParameters.getEventFactory().createStartElement(
                creationalParameters.getPrefix(), creationalParameters.getNamespaceUri(), localName, attributeList.iterator(), null));
        events.add(creationalParameters.getEventFactory().createEndElement(
                creationalParameters.getPrefix(), creationalParameters.getNamespaceUri(), localName));

        return new RunProperty.GenericRunProperty(events);
    }

    /**
     * Provides toggle property names.
     */
    enum TogglePropertyName {
        UNSUPPORTED(""),

        BOLD("b"),
        COMPLEX_SCRIPT_BOLD("bCs"),
        CAPS("caps"),
        EMBOSS("emboss"),
        ITALICS("i"),
        COMPLEX_SCRIPT_ITALICS("iCs"),
        IMPRINT("imprint"),
        OUTLINE("outline"),
        SHADOW("shadow"),
        SMALL_CAPS("smallCaps"),
        STRIKE_THROUGH("strike"),
        VANISH("vanish");

        QName value;

        TogglePropertyName(String value) {
            this.value = WordProcessingML.getQName(value);
        }

        QName getValue() {
            return value;
        }

        static TogglePropertyName fromValue(QName value) {
            if (null == value) {
                return UNSUPPORTED;
            }

            for (TogglePropertyName propertyName : values()) {
                if (propertyName.getValue().equals(value)) {
                    return propertyName;
                }
            }

            return UNSUPPORTED;
        }
    }
}
