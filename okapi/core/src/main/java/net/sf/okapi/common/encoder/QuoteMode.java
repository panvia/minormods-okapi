package net.sf.okapi.common.encoder;

/**
 * Specifies the behavior for the escaping of single and double quotes. 
 */
public enum QuoteMode {
	
	/**
	 * Do not escape single or double quotes.
	 */
	UNESCAPED(0),
	/**
	 * Escape single and double quotes to a named entity.
	 */
	ALL(1),
	/**
	 * Escape double quotes to a named entity, and single quotes to a numeric entity.
	 */
	NUMERIC_SINGLE_QUOTES(2),
	/**
	 * Escape double quotes only.
	 */
	DOUBLE_QUOTES_ONLY(3);

	// This is identical to ordinal, but protects us from accident rearrangement.
	private int value;
	QuoteMode(int value) {
		this.value = value;
	}

	public static QuoteMode fromValue(int value) {
		for (QuoteMode qm : values()) {
			if (value == qm.value) return qm;
		}
		throw new IllegalArgumentException("Invalid quote mode: " + value);
	}
}