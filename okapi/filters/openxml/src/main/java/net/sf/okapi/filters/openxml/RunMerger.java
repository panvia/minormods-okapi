package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static net.sf.okapi.filters.openxml.XMLEventHelpers.addChunksToList;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isWhitespace;

class RunMerger {
    private String paragraphStyle;
    private RunBuilder previousRunBuilder;
    private List<Block.BlockChunk> completedRuns = new ArrayList<>();

    void setParagraphStyle(String paragraphStyle) {
        this.paragraphStyle = paragraphStyle;
    }

    List<Block.BlockChunk> getRuns() throws XMLStreamException {
        if (previousRunBuilder != null) {
            completedRuns.add(previousRunBuilder.build());
            previousRunBuilder = null;
        }

        return completedRuns;
    }

    void add(RunBuilder runBuilder) throws XMLStreamException {
        if (null == previousRunBuilder) {
            runBuilder.resetCombinedRunProperties(paragraphStyle); // to be consistent with other run builders
            previousRunBuilder = runBuilder;

            return;
        }

        if (canMerge(previousRunBuilder, runBuilder)) {
            merge(previousRunBuilder, runBuilder);
        } else {
            completedRuns.add(previousRunBuilder.build());
            previousRunBuilder = runBuilder;
        }
    }

    private boolean canMerge(RunBuilder runBuilder, RunBuilder otherRunBuilder) {

        RunProperties combinedRunProperties = runBuilder.getCombinedRunProperties(paragraphStyle);
        RunProperties combinedOtherRunProperties = otherRunBuilder.getCombinedRunProperties(paragraphStyle);

        // Merging runs in the math namespace can sometimes corrupt formulas,
        // so don't do it.
        if (Namespaces.Math.containsName(runBuilder.getStartElementContext().getStartElement().getName())) {
            return false;
        }
        // XXX Don't merge runs that have nested blocks, to avoid having to go
        // back and renumber the references in the skeleton. I should probably
        // fix this at some point.  Note that we check for the existence of -any-
        // nested block, not just ones with text.
        // The reason for this pre-caution is because when we merge runs, we
        // re-parse the xml eventReader.  However, it doesn't cover all the cases.
        // We might be able to remove this restriction if we could clean up the
        // way the run body eventReader are parsed during merging.
        if (runBuilder.containsNestedItems() || otherRunBuilder.containsNestedItems()) {
            return false;
        }
        // Don't merge stuff involving complex codes
        if (runBuilder.hasComplexCodes() || otherRunBuilder.hasComplexCodes()) {
            return false;
        }

        return canRunPropertiesBeMerged(combinedRunProperties, combinedOtherRunProperties);
    }

    private boolean canRunPropertiesBeMerged(RunProperties currentProperties, RunProperties otherProperties) {
        if (currentProperties.count() != otherProperties.count()) {
            return false;
        }

        int numberOfMatchedProperties = 0;

        for (RunProperty currentProperty : currentProperties.getProperties()) {
            QName currentPropertyStartElementName = currentProperty.getName();

            for (RunProperty otherProperty : otherProperties.getProperties()) {
                QName otherPropertyStartElementName = otherProperty.getName();

                if (!currentPropertyStartElementName.equals(otherPropertyStartElementName)) {
                    continue;
                }

                if (currentProperty instanceof MergeableRunProperty && otherProperty instanceof MergeableRunProperty) {
                    if (!((MergeableRunProperty) currentProperty).canBeMerged((MergeableRunProperty) otherProperty)) {
                        return false;
                    }
                } else {
                    if (!currentProperty.canBeReplaced(otherProperty)) {
                        return false;
                    }
                }
                numberOfMatchedProperties++;
                break;
            }
        }

        if (numberOfMatchedProperties < currentProperties.count()) {
            return false;
        }

        return true;
    }

    /**
     * Merge the property body.  This is something of a mess, since
     * this may cause tetris-style collapsing of consecutive text across
     * the two runs. To handle this correctly, we concatenate the two run
     * bodies, then re-process them.
     */
    void merge(RunBuilder runBuilder, RunBuilder otherRunBuilder) throws XMLStreamException {
        runBuilder.setRunProperties(mergeRunProperties(runBuilder.getRunProperties(), otherRunBuilder.getRunProperties()));
        runBuilder.resetCombinedRunProperties(paragraphStyle);

        List<XMLEvent> newBodyEvents = new ArrayList<>();
        addChunksToList(newBodyEvents, runBuilder.getRunBodyChunks());
        runBuilder.setTextPreservingWhitespace(runBuilder.isTextPreservingWhitespace() || otherRunBuilder.isTextPreservingWhitespace());
        addChunksToList(newBodyEvents, otherRunBuilder.getRunBodyChunks());
        runBuilder.getRunBodyChunks().clear();
        XMLEventReader events = new XMLListEventReader(newBodyEvents);
        while (events.hasNext()) {
            runBuilder.addRunBody(events.nextEvent(), events);
        }
        // Flush any outstanding buffers
        runBuilder.flushText();
        runBuilder.flushMarkupChunk();
    }

    private RunProperties mergeRunProperties(RunProperties runProperties, RunProperties otherRunProperties) {
        // try to reduce the set of properties
        if (0 == runProperties.count() && 0 == otherRunProperties.count()) {
            return runProperties;
        }

        List<RunProperty> mergeableRunProperties = runProperties.getMergeableRunProperties();

        if (mergeableRunProperties.isEmpty()) {
            return otherRunProperties;
        }

        List<RunProperty> otherMergeableRunProperties = otherRunProperties.getMergeableRunProperties();

        if (otherMergeableRunProperties.isEmpty()) {
            return runProperties;
        }

        if (mergeableRunProperties.size() >= otherMergeableRunProperties.size()) {
            List<RunProperty> remainedOtherMergeableRunProperties = mergeMergeableRunProperties(mergeableRunProperties, otherMergeableRunProperties);
            runProperties.getProperties().addAll(remainedOtherMergeableRunProperties);

            return runProperties;
        }

        List<RunProperty> remainedMergeableRunProperties = mergeMergeableRunProperties(otherMergeableRunProperties, mergeableRunProperties);
        otherRunProperties.getProperties().addAll(remainedMergeableRunProperties);

        return otherRunProperties;
    }

    private List<RunProperty> mergeMergeableRunProperties(List<RunProperty> mergeableRunProperties, List<RunProperty> otherMergeableRunProperties) {
        List<RunProperty> remainedOtherMergeableRunProperties = new ArrayList<>(otherMergeableRunProperties);

        for (RunProperty runProperty : mergeableRunProperties) {
            QName currentPropertyStartElementName = runProperty.getName();

            Iterator<RunProperty> remainedOtherMergeableRunPropertyIterator = remainedOtherMergeableRunProperties.iterator();

            while (remainedOtherMergeableRunPropertyIterator.hasNext()) {
                RunProperty otherRunProperty = remainedOtherMergeableRunPropertyIterator.next();
                QName otherPropertyStartElementName = otherRunProperty.getName();

                if (!currentPropertyStartElementName.equals(otherPropertyStartElementName)) {
                    continue;
                }

                ((MergeableRunProperty) runProperty).merge((MergeableRunProperty) otherRunProperty);
                remainedOtherMergeableRunPropertyIterator.remove();
                break;
            }
        }

        return remainedOtherMergeableRunProperties;
    }

    void reset() {
        completedRuns.clear();
        previousRunBuilder = null;
    }

    /**
     * Wrap an event iterator as a true reader; this lets us replay strings
     * of eventReader during merge.
     */
    class XMLListEventReader implements XMLEventReader {
        private Iterator<XMLEvent> events;

        XMLListEventReader(Iterable<XMLEvent> events) {
            this.events = events.iterator();
        }

        @Override
        public boolean hasNext() {
            return events.hasNext();
        }

        @Override
        public XMLEvent nextEvent() {
            return events.next();
        }

        @Override
        public void remove() {
            events.remove();
        }

        @Override
        public XMLEvent nextTag() throws XMLStreamException {
            for (XMLEvent e = nextEvent(); e != null; e = nextEvent()) {
                if (e.isStartElement() || e.isEndElement()) {
                    return e;
                } else if (!isWhitespace(e)) {
                    throw new IllegalStateException("Unexpected event: " + e);
                }
            }
            return null;
        }

        @Override
        public XMLEvent peek() throws XMLStreamException {
            throw new UnsupportedOperationException();
        }

        @Override
        public String getElementText() throws XMLStreamException {
            throw new UnsupportedOperationException();
        }

        @Override
        public Object getProperty(String name) throws IllegalArgumentException {
            throw new UnsupportedOperationException();
        }

        @Override
        public void close() throws XMLStreamException {
        }

        @Override
        public Object next() {
            return events.next();
        }
    }
}