package net.sf.okapi.filters.openxml;

import java.util.List;

class RunBuilderSkipper implements Skipper<RunBuilder> {

    public boolean canBeSkipped(RunBuilder runBuilder) {
        if (!runBuilder.getNestedTextualItems().isEmpty()) {
            return false;
        }

        List<Chunk> runBodyChunks = runBuilder.getRunBodyChunks();
        if (runBodyChunks.isEmpty()) {
            return true;
        }

        for (Chunk runBodyChunk : runBodyChunks) {
            if (runBodyChunk instanceof Run.RunText && ((Run.RunText) runBodyChunk).getText().isEmpty()){
                continue;
            }
            return false;
        }

        return true;
    }
}
